package com.bank.transfer.internal.event;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.UUID;

@Setter
@Getter
public class TransactionEvent {
    private String sagaId;
    private UUID clientTransactionId;
    private UUID productId;
    private String productCode;
    private String fromCif;
    private String beneficiary;
    private BigDecimal amount;
    private String status; //INIT, SUCCESS, FAIL
}
